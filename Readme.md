Technical assignment of TurboTrader.

- TTObjectManager - created as a template to make API calls
- TTQuestion and descendants - the question object used to collect basic user info, currently supported 3 types
(single select, date and text)
- TTQuestionGroup - a group of questions assigned to a specific country (expected to receieve using an API call)
- Used a small delay within the template API calls.

Mostly the list of questions doesn't matter, since the server should return a valid guestion group. The requested templates are present in - TTObjectManager. 

Technical Specs:
1. iOS 7 and Above
2. Manual Reference Counting
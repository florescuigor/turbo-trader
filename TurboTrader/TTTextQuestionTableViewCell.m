// ---------------------------------------------------------------------------------------------------------------------

#import "TTTextQuestionTableviewCell.h"
#import "TTTableViewDelegate.h"
#import "TTAnswer.h"

// ---------------------------------------------------------------------------------------------------------------------

#define TTMinimumHeight 54
#define TTMaximumHeight 200

@interface TTTextQuestionTableViewCell () <UITextViewDelegate>
@property (retain, nonatomic) IBOutlet UITextView *textView;
@end

@implementation TTTextQuestionTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self.textView setDelegate:self];
}

- (void)dealloc
{
    [_textView release];
    [super dealloc];
}

- (void)setAnswer:(TTAnswer *)answer
{
    [super setAnswer:answer];
    
    self.textView.text = answer.value;
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView
{
    UITableView *tableView = [self tableView];
    if (tableView) {
        id <TTTableViewDelegate> delegate = (id <TTTableViewDelegate> )tableView.delegate;
        [delegate tableView:tableView didChangeAnswerValue:textView.text forCell:self];
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    UITableView *tableView = [self tableView];
    NSIndexPath *indexPath = [tableView indexPathForCell:self];
    [[self tableView] scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

#pragma mark - helpers

- (UITableView *)tableView
{
    id view = [self superview];
    while (view && [view isKindOfClass:[UITableView class]] == NO) {
        view = [view superview];
    }
    return view;
}

@end
// ---------------------------------------------------------------------------------------------------------------------

#import "TTPopoverSegue.h"

// ---------------------------------------------------------------------------------------------------------------------

@implementation TTPopoverSegue

- (void)perform
{
    UIViewController *sourceViewController = self.sourceViewController;
    UIViewController *destinationViewController = self.destinationViewController;
    
    CGPoint origin = sourceViewController.view.center;
    
    [sourceViewController.view addSubview:destinationViewController.view];
    
    CGPoint originalCenter = destinationViewController.view.center;
    destinationViewController.view.center = origin;
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         destinationViewController.view.center = originalCenter;
                     }
                     completion:^(BOOL finished){
                         
                         [destinationViewController.view removeFromSuperview];
                         [sourceViewController presentViewController:destinationViewController
                                                            animated:NO
                                                          completion:NULL];
                     }];
}

@end

// ---------------------------------------------------------------------------------------------------------------------

#import "TTOption.h"
#import "TTCommon.h"

// ---------------------------------------------------------------------------------------------------------------------

@implementation TTOption

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSNumber *uniqueValue = [[NSUserDefaults standardUserDefaults] objectForKey:kTTLocalUniqueOptionKey];
        _value = uniqueValue.integerValue + 1;
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:_value] forKey:kTTLocalUniqueOptionKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    return self;
}

- (void)dealloc
{
    self.title = nil;
    
    [super dealloc];
}

@end
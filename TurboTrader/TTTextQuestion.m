// ---------------------------------------------------------------------------------------------------------------------

#import "TTTextQuestion.h"

// ---------------------------------------------------------------------------------------------------------------------

@implementation TTTextQuestion

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.questionType = TTQuestionTypeText;
    }
    return self;
}

@end
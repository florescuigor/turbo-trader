//
//  main.m
//  TurboTrader
//
//  Created by Florescu Igor on 6/2/15.
//  Copyright (c) 2015 Florescu Igor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

// ---------------------------------------------------------------------------------------------------------------------

#import "TTSingleSelectQuestion.h"

// ---------------------------------------------------------------------------------------------------------------------

@implementation TTSingleSelectQuestion

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.questionType = TTQuestionTypeSingleSelect;
    }
    return self;
}

- (void)dealloc
{
    self.options = nil;
    
    [super dealloc];
}

@end

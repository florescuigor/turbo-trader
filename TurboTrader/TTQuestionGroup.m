// ---------------------------------------------------------------------------------------------------------------------

#import "TTQuestionGroup.h"

// ---------------------------------------------------------------------------------------------------------------------

@implementation TTQuestionGroup

- (void)dealloc
{
    self.questions = nil;
    
    [super dealloc];
}

@end

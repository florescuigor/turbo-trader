// ---------------------------------------------------------------------------------------------------------------------

#import "TTDateTableViewCell.h"
#import "TTAnswer.h"

// ---------------------------------------------------------------------------------------------------------------------

@implementation TTDateTableViewCell

- (void)setAnswer:(TTAnswer *)answer
{
    [super setAnswer:answer];
    
    if (answer.value.length > 0) {
        NSTimeInterval timeInterval = [answer.value doubleValue];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
        
        self.textLabel.text = [date descriptionWithLocale:[NSLocale currentLocale]];
    } else {
        self.textLabel.text = nil;
    }
}

@end

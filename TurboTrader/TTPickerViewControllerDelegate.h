// ---------------------------------------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>

// ---------------------------------------------------------------------------------------------------------------------

@class TTPickerViewController;

// The Date picker delegate
@protocol TTPickerViewControllerDelegate <NSObject>
// Retuns the selected date by user
- (void)viewController:(TTPickerViewController *)controller didSelectDate:(NSDate *)date;
@end
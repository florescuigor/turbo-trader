// ---------------------------------------------------------------------------------------------------------------------

#import "TTQuestion.h"

// ---------------------------------------------------------------------------------------------------------------------

@implementation TTQuestion

- (instancetype)init
{
    self = [super init];
    if (self) {
        _questionType = TTQuestionTypeDefault;
        
        NSNumber *uniqueValue = [[NSUserDefaults standardUserDefaults] objectForKey:kTTLocalUniqueNumberKey];
        _questionID = uniqueValue.integerValue + 1;
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:_questionID] forKey:kTTLocalUniqueNumberKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    return self;
}

- (void)dealloc
{
    self.title = nil;
    
    [super dealloc];
}

@end
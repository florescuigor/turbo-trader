// ---------------------------------------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>

// ---------------------------------------------------------------------------------------------------------------------

// An add option, used for select questions
@interface TTOption : NSObject
@property(nonatomic, retain)NSString *title;
@property(nonatomic, readwrite)NSInteger value;
@end

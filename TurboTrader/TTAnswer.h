// ---------------------------------------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>

// ---------------------------------------------------------------------------------------------------------------------

// The end up answer object collected from the user
@interface TTAnswer : NSObject

// Used to identify the question what its tied up
@property(nonatomic, readwrite)NSInteger questionID;

// The actual value, it may be an option identifier, timestamp or whatever data the server is expecting
@property(nonatomic, retain)NSString *value;

@end

// ---------------------------------------------------------------------------------------------------------------------

#import "TTObjectManager.h"

#import "TTSingleSelectQuestion.h"
#import "TTOption.h"

#import "TTQuestionGroup.h"
#import "TTTextQuestion.h"
#import "TTDateQuestion.h"
#import "TTUser.h"

// ---------------------------------------------------------------------------------------------------------------------

@implementation TTObjectManager

+ (TTObjectManager *)sharedInstance
{
    static TTObjectManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[TTObjectManager alloc] init];
    });
    
    return sharedInstance;
}

- (void)dealloc
{
    [super dealloc];
}

#pragma mark - public methods

- (void)signupForUserAccountUsingCredentials:(NSArray *)credentials completion:(TTRequestSignupResultBlock)completion
{
    // 1. Perform the API call
    // 2. Serialize the required credentials
    // 3. Handle the completion call as expected
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        // Assuming that the server returned asuccesfull response, pass the current user
        TTUser *user = [[TTUser alloc] init];
        [user setCredentials:credentials];
        
        completion(user, nil);
    });
}

- (void)countries:(TTRequestCountriesQuestionResultBlock)completion
{
    // Testing behaviour
    // Normaly there should be a an API call, gather the data, deserialize and fire the completion
    // Using delay to fire the completion, generate a fake impresion that some data gets processed or downloaded
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        TTSingleSelectQuestion *question = [self countryQuestion];
        
        completion(question, nil);
    });
}

- (void)questionGroupForCountryCode:(TTCountryCode)countryCode completion:(TTRequestQuestionGroupResultBlock)completion
{
    // Testing behaviour
    // Normaly there should be a an API call, gather the data, deserialize and fire the completion
    // Using delay to fire the completion, generate a fake impresion that some data gets processed or downloaded
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        // The testing question group, currently a static implementation unitl the API call
        TTQuestionGroup *questionGroup = [self questionGroupUsingCountryCode:countryCode];
    
        // After the data was received, fire up the completion for the app further processing
        completion(questionGroup, nil);
    });
}

#pragma mark - testing templates methods

// Testing template metods used to create the desired questions.

- (TTQuestionGroup *)questionGroupUsingCountryCode:(TTCountryCode)countryCode
{
    TTQuestionGroup *questionGroup = [[[TTQuestionGroup alloc] init] autorelease];
    
    NSMutableArray *basicQuestions = [NSMutableArray arrayWithArray:[self basicQuestions]];
    
    if (countryCode == TTCountryCodeUnitedKingdom) {
        
    }  else if (countryCode == TTCountryCodeUnitedStates) {
        
        TTTextQuestion *textQuestion = [[[TTTextQuestion alloc] init] autorelease];
        textQuestion.title = @"Type a long description of your previos job";
        textQuestion.required = YES;
        [basicQuestions addObject:textQuestion];
        
        TTDateQuestion *dateQuestion = [[[TTDateQuestion alloc] init] autorelease];
        dateQuestion.title = @"When did you begin your last job?";
        dateQuestion.required = YES;
        [basicQuestions addObject:dateQuestion];
        
        dateQuestion = [[[TTDateQuestion alloc] init] autorelease];
        dateQuestion.title = @"When did you finish your last job?";
        dateQuestion.required = YES;
        [basicQuestions addObject:dateQuestion];
        
    }  else if (countryCode == TTCountryCodeGermany) {
        
        TTSingleSelectQuestion *incomeQuestion = [[[TTSingleSelectQuestion alloc] init] autorelease];
        incomeQuestion.title = @"Please choose your annual financial status";
        incomeQuestion.required = NO;
        
        TTOption *option1 = [[[TTOption alloc] init] autorelease];
        option1.title = @" Less than 50000 Euro";
        
        TTOption *option2 = [[[TTOption alloc] init] autorelease];
        option2.title = @"Within 50000 and 80000";
        
        TTOption *option3 = [[[TTOption alloc] init] autorelease];
        option3.title = @"Within 80000 and 120000";

        TTOption *option4 = [[[TTOption alloc] init] autorelease];
        option4.title = @" More than 120000";
        
        incomeQuestion.options = @[option1, option2, option3, option4];
        [basicQuestions addObject:incomeQuestion];
        
    }
    
    questionGroup.questions = basicQuestions;
    
    return questionGroup;
}

- (NSArray *)basicQuestions
{
    NSMutableArray *questions = [NSMutableArray array];
    
    TTTextQuestion *nameQuestion = [[[TTTextQuestion alloc] init] autorelease];
    nameQuestion.title = @"First name";
    nameQuestion.required = YES;
    [questions addObject:nameQuestion];
    
    TTTextQuestion *phoneQuestion = [[[TTTextQuestion alloc] init] autorelease];
    phoneQuestion.title = @"Last name";
    phoneQuestion.required = YES;
    [questions addObject:phoneQuestion];
    
    TTTextQuestion *emailQuestion = [[[TTTextQuestion alloc] init] autorelease];
    emailQuestion.title = @"e-mail address";
    emailQuestion.required = NO;
    [questions addObject:emailQuestion];
    
    TTDateQuestion *dateQuestion = [[[TTDateQuestion alloc] init] autorelease];
    dateQuestion.title = @"Date of birth";
    dateQuestion.required = YES;
    [questions addObject:dateQuestion];
    
    TTSingleSelectQuestion *genderQuestion = [[[TTSingleSelectQuestion alloc] init] autorelease];
    genderQuestion.title = @"Choose your gender";
    genderQuestion.required = NO;
    
    TTOption *maleOption = [[[TTOption alloc] init] autorelease];
    maleOption.title = @"Male";
    
    TTOption *femaleOption = [[[TTOption alloc] init] autorelease];
    femaleOption.title = @"Female";
    
    genderQuestion.options = @[maleOption, femaleOption];
    [questions addObject:genderQuestion];
    
    return [NSArray arrayWithArray:questions];
}

// Country options
- (TTSingleSelectQuestion *)countryQuestion
{
    TTSingleSelectQuestion *question = [[[TTSingleSelectQuestion alloc] init] autorelease];
    question.options = [self countryOptions];
    
    return question;
}

- (NSArray *)countryOptions
{
    NSMutableArray *countries = [NSMutableArray array];
    
    TTOption *unitedKingdomOption = [[[TTOption alloc] init] autorelease];
    unitedKingdomOption.title = @"United Kingdom";
    unitedKingdomOption.value = TTCountryCodeUnitedKingdom;
    [countries addObject:unitedKingdomOption];
    
    TTOption *unitedStatesOption = [[[TTOption alloc] init] autorelease];
    unitedStatesOption.title = @"United States";
    unitedStatesOption.value = TTCountryCodeUnitedStates;
    [countries addObject:unitedStatesOption];
    
    TTOption *germanyOption = [[[TTOption alloc] init] autorelease];
    germanyOption.title = @"Germany";
    germanyOption.value = TTCountryCodeGermany;
    [countries addObject:germanyOption];
    
//    TTOption *franceOption = [[[TTOption alloc] init] autorelease];
//    franceOption.title = @"France";
//    franceOption.value = TTCountryCodeFrance;
//    [countries addObject:franceOption];
    
    return [NSArray arrayWithArray:countries];
}

@end
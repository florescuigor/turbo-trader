// ---------------------------------------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>

// ---------------------------------------------------------------------------------------------------------------------

// Questions are distinguished by types, currently text, single select and date
typedef NS_OPTIONS(NSInteger, TTQuestionType) {
    TTQuestionTypeDefault      = 0,
    TTQuestionTypeText         = 1,
    TTQuestionTypeSingleSelect = 2,
    TTQuestionTypeDate         = 3
};

// Valid country codes
typedef NS_OPTIONS(NSInteger, TTCountryCode) {
    TTCountryCodeDefault       = 0,
    TTCountryCodeUnitedStates  = 1,
    TTCountryCodeUnitedKingdom = 44,
    TTCountryCodeGermany       = 49,
    TTCountryCodeFrance        = 45
};

@class TTSingleSelectQuestion;
@class TTQuestionGroup;
@class TTUser;

typedef void (^TTRequestSignupResultBlock)(TTUser *user, NSError *error);
typedef void (^TTRequestCountriesQuestionResultBlock)(TTSingleSelectQuestion *question, NSError *error);
typedef void (^TTRequestQuestionGroupResultBlock)(TTQuestionGroup *questionGroup, NSError *error);

// Segue constants
extern NSString *const kTTSegueForCredentialCollection;
extern NSString *const kTTSequeForDatePickerCollection;

// Local preferences settings
extern NSString *const kTTLocalUniqueNumberKey;
extern NSString *const kTTLocalUniqueOptionKey;
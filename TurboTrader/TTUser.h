// ---------------------------------------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>

// ---------------------------------------------------------------------------------------------------------------------

@interface TTUser : NSObject
// The data used by the user to sign in
@property(nonatomic, retain)NSArray *credentials;
@end

// ---------------------------------------------------------------------------------------------------------------------

#import "TTCountryViewController.h"
#import "TTUserCreationController.h"
#import "TTSingleSelectQuestion.h"
#import "TTObjectManager.h"

// ---------------------------------------------------------------------------------------------------------------------

@interface TTCountryViewController ()
@property (nonatomic, retain) NSArray *results;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, retain) TTQuestionGroup *questionGroup;
@end

@implementation TTCountryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self updateCountries];
}

- (void)dealloc
{
    self.results = nil;
    self.questionGroup = nil;
    
    [_activityIndicator release];
    
    [super dealloc];
}

- (void)updateCountries
{
    [self.activityIndicator startAnimating];
    
    // Load the list of the available countries from the server
    [[TTObjectManager sharedInstance] countries:^(TTSingleSelectQuestion *question, NSError *error) {
        
        [self.activityIndicator stopAnimating];
        if (error) {
            // Handle the error
        } else {
            self.results = question.options;
            [self.tableView reloadData];
        }
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kTTSegueForCredentialCollection]) {
        TTUserCreationController *signupController = segue.destinationViewController;
        [signupController setQuestionGroup:self.questionGroup];
    }
}

- (void)didSelectCountryCode:(TTCountryCode)countryCode
{
    [self.activityIndicator startAnimating];
    
    // Load the question group for the selected country code, and push the registration controller
    [[TTObjectManager sharedInstance] questionGroupForCountryCode:countryCode completion:^(TTQuestionGroup *questionGroup, NSError *error) {
        [self.activityIndicator stopAnimating];
        if (error) {
            // Handle the error
        } else {
            self.questionGroup = questionGroup;
            [self performSegueWithIdentifier:kTTSegueForCredentialCollection sender:self];
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.results.count;
}

#pragma mark - Table view delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"] autorelease];
    }
    
    TTOption *option = self.results[indexPath.row];
    cell.textLabel.text = option.title;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TTOption *option = self.results[indexPath.row];
    [self didSelectCountryCode:option.value];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64;
}

@end
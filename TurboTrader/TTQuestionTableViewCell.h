// ---------------------------------------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>

// ---------------------------------------------------------------------------------------------------------------------

@class TTAnswer;

// The Base Question cell
@interface TTQuestionTableViewCell : UITableViewCell
@property(nonatomic, retain)TTAnswer *answer;
@end

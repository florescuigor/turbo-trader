// ---------------------------------------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>

// ---------------------------------------------------------------------------------------------------------------------

@class TTQuestion;
@class TTAnswer;
@class TTQuestionTableViewCell;

@interface TTTableViewCellFactory : NSObject

// Returns a reuse identifier for a specified question
+ (NSString *)reuseIdentifierForQuestion:(TTQuestion *)question;

// Returns number of rows for a section, currently more than 1 for single select
+ (NSInteger)numberOfRowsForQuestion:(TTQuestion *)question;

// Calculates the height of the cell regarding the answer of the user, within the current layout
+ (CGFloat)heightForRowUsingQuestion:(TTQuestion *)question
                           andAnswer:(TTAnswer *)answer
                           tableView:(UITableView *)tableView;

+ (TTQuestionTableViewCell *)questionCellUsingQuestion:(TTQuestion *)question;

@end

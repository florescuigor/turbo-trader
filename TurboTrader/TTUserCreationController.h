// ---------------------------------------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>

// ---------------------------------------------------------------------------------------------------------------------

@class TTQuestionGroup;

@interface TTUserCreationController : UITableViewController
// A list of questions tied up specificaly for an country code
@property (nonatomic, retain)TTQuestionGroup *questionGroup;
@end

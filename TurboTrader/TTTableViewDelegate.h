// ---------------------------------------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>

// ---------------------------------------------------------------------------------------------------------------------

@class TTQuestionTableViewCell;

// Extended protocol of the table view delegate, currently aplicable only when text cell changes the text, in order to
// resize the contents of the calling cell
@protocol TTTableViewDelegate <UITableViewDelegate>
@required
- (void)tableView:(UITableView *)tableView didChangeAnswerValue:(NSString *)value forCell:(TTQuestionTableViewCell *)cell;
@end
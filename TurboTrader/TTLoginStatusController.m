// ---------------------------------------------------------------------------------------------------------------------

#import "TTLoginStatusController.h"

// ---------------------------------------------------------------------------------------------------------------------

@interface TTLoginStatusController ()
@property (retain, nonatomic) IBOutlet UIButton *loginButton;
@end

@implementation TTLoginStatusController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.loginButton.layer.cornerRadius = 5;
}

- (void)dealloc
{
    [_loginButton release];
    [super dealloc];
}

@end

// ---------------------------------------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>

// ---------------------------------------------------------------------------------------------------------------------

@interface TTQuestionGroup : NSObject

@property(nonatomic, strong)NSArray *questions;

@end

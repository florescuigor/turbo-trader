// ---------------------------------------------------------------------------------------------------------------------

#import "TTAnswer.h"

// ---------------------------------------------------------------------------------------------------------------------

@implementation TTAnswer

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ - questionID[%ld], value[%@]", NSStringFromClass([self class]), (long)self.questionID, self.value];
}

- (void)dealloc
{
    self.value = nil;
    [super dealloc];
}

@end

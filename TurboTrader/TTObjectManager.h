// ---------------------------------------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>
#import "TTCommon.h"

// ---------------------------------------------------------------------------------------------------------------------

@interface TTObjectManager : NSObject

+ (TTObjectManager *)sharedInstance;

// Creates an user account using the collected credentials, completion returns TTUser created
- (void)signupForUserAccountUsingCredentials:(NSArray *)credentials completion:(TTRequestSignupResultBlock)completion;

// Succesfull completion returns a TTSingleSelectQuestion, defining what countries are currently available for signup
- (void)countries:(TTRequestCountriesQuestionResultBlock)completion;

// Each country code has its own question group assigned, the completion returns a TTQuestionGroup assigned for the
// specified country code, afther the api call
- (void)questionGroupForCountryCode:(TTCountryCode)countryCode completion:(TTRequestQuestionGroupResultBlock)completion;

@end

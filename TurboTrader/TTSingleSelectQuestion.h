// ---------------------------------------------------------------------------------------------------------------------

#import "TTQuestion.h"
#import "TTOption.h"

// ---------------------------------------------------------------------------------------------------------------------

@interface TTSingleSelectQuestion : TTQuestion
@property(nonatomic, retain)NSArray *options;
@end

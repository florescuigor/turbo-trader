// ---------------------------------------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>
#import "TTCommon.h"

// ---------------------------------------------------------------------------------------------------------------------

@interface TTQuestion : NSObject

// Generaly defined question type, used to distinguish the question default values or UI building
@property(nonatomic, readwrite)TTQuestionType questionType;
// An unique identifier of the question object
@property(nonatomic, readwrite)NSInteger      questionID;
// Defines if the question is required to be answered by the user in order to continue the registration process
@property(nonatomic, readwrite)BOOL           required;

@property(nonatomic, retain)NSString *title;

@end

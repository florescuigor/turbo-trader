// ---------------------------------------------------------------------------------------------------------------------

#import "TTQuestion.h"

// ---------------------------------------------------------------------------------------------------------------------

@interface TTTextQuestion : TTQuestion
@property(nonatomic, readwrite)NSUInteger minimumLenght;
@property(nonatomic, readwrite)NSUInteger maximumLenght;
@end
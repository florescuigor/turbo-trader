// ---------------------------------------------------------------------------------------------------------------------

#import "TTCommon.h"

// ---------------------------------------------------------------------------------------------------------------------

NSString *const kTTSegueForCredentialCollection = @"userCreationSegue";
NSString *const kTTSequeForDatePickerCollection = @"pickerSegue";
NSString *const kTTLocalUniqueNumberKey         = @"kTTLocalUniqueNumberKey";
NSString *const kTTLocalUniqueOptionKey         = @"kTTLocalUniqueOptionKey";
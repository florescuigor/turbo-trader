// ---------------------------------------------------------------------------------------------------------------------

#import "TTDateQuestion.h"

// ---------------------------------------------------------------------------------------------------------------------

@implementation TTDateQuestion

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.questionType = TTQuestionTypeDate;
    }
    return self;
}

- (void)dealloc
{
    self.minimumDate = nil;
    self.maximumDate = nil;
    
    [super dealloc];
}

@end

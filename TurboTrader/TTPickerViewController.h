// ---------------------------------------------------------------------------------------------------------------------

#import <UIKit/UIKit.h>
#import "TTPickerViewControllerDelegate.h"
#import "TTCommon.h"

// ---------------------------------------------------------------------------------------------------------------------

@interface TTPickerViewController : UIViewController
@property(nonatomic, retain)NSDate *minimumDate;
@property(nonatomic, retain)NSDate *maximumDate;
@property(nonatomic, assign)id <TTPickerViewControllerDelegate> delegate;
@end

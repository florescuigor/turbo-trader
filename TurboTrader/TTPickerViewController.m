// ---------------------------------------------------------------------------------------------------------------------

#import "TTPickerViewController.h"

// ---------------------------------------------------------------------------------------------------------------------

@interface TTPickerViewController ()
@property (retain, nonatomic) IBOutlet UIView *contentView;
@property (retain, nonatomic) IBOutlet UIView *titleLabel;
@property (retain, nonatomic) IBOutlet UIView *rightButton;
@property (retain, nonatomic) IBOutlet UIView *leftBUtton;
@property (retain, nonatomic) IBOutlet UIDatePicker *datePicker;
@end

@implementation TTPickerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.datePicker.datePickerMode = UIDatePickerModeDate;
}

- (void)dealloc
{
    self.delegate = nil;
    self.minimumDate = nil;
    self.maximumDate = nil;
    
    [_contentView release];
    [_titleLabel release];
    [_rightButton release];
    [_leftBUtton release];
    [_datePicker release];
    
    [super dealloc];
}

#pragma mark - setters

- (void)setMinimumDate:(NSDate *)minimumDate
{
    if (_minimumDate) {
        [_minimumDate release];
    }
    _minimumDate = minimumDate;
    
    if (_minimumDate) {
        self.datePicker.minimumDate = _minimumDate;
    } else {
        self.datePicker.minimumDate = nil;
    }
}

- (void)setMaximumDate:(NSDate *)maximumDate
{
    if (_maximumDate) {
        [_maximumDate release];
    }
    _maximumDate = maximumDate;
    
    if (_maximumDate) {
        self.datePicker.maximumDate = _maximumDate;
    } else {
        self.datePicker.maximumDate = [NSDate date];
    }
}

#pragma mark - 

- (void)dismiss
{
    // Temporary
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didSelectCancelButton:(id)sender
{
    [self dismiss];
}

- (IBAction)didSelectDoneButton:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(viewController:didSelectDate:)]) {
        [self.delegate viewController:self didSelectDate:self.datePicker.date];
    }
    [self dismiss];
}

@end
// ---------------------------------------------------------------------------------------------------------------------

#import "TTUserCreationController.h"
#import "TTPickerViewController.h"
#import "TTTableViewCellFactory.h"
#import "TTQuestionTableViewCell.h"
#import "TTSingleSelectQuestion.h"
#import "TTTableViewDelegate.h"
#import "TTObjectManager.h"
#import "TTQuestionGroup.h"
#import "TTDateQuestion.h"
#import "TTQuestion.h"
#import "TTAnswer.h"
#import "TTOption.h"

// ---------------------------------------------------------------------------------------------------------------------

@interface TTUserCreationController () <TTTableViewDelegate, TTPickerViewControllerDelegate>
@property(nonatomic, retain)NSMutableArray *answers;
@property(nonatomic, assign)TTQuestion *currentQuestion;
@end

@implementation TTUserCreationController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.answers = [NSMutableArray array];
    [self.tableView setBackgroundColor:[UIColor colorWithRed:.94 green:.94 blue:.94 alpha:1]];
}

- (void)dealloc
{
    self.currentQuestion = nil;
    [_answers release];
    [super dealloc];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kTTSequeForDatePickerCollection]) {
        TTDateQuestion *dateQuestion = (TTDateQuestion *)self.currentQuestion;
        TTPickerViewController *viewController = segue.destinationViewController;
        viewController.minimumDate = dateQuestion.minimumDate;
        viewController.maximumDate = dateQuestion.maximumDate;
        [viewController setDelegate:self];
    }
}

- (IBAction)didSelectSubmitButton:(id)sender
{
    NSArray *answers = self.answers;
    
    [[TTObjectManager sharedInstance] signupForUserAccountUsingCredentials:answers completion:^(TTUser *user, NSError *error) {
        if (error) {
            // Handle the error case
        } else {
           
            [[[[UIAlertView alloc] initWithTitle:@"Succeded"
                                        message:nil
                                       delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil] autorelease] show];
        }
    }];
}

#pragma mark - setters

- (void)setQuestionGroup:(TTQuestionGroup *)questionGroup
{
    if (_questionGroup) {
        [_questionGroup release];
    }
    _questionGroup = questionGroup;
    
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.questionGroup.questions.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    TTQuestion *question = self.questionGroup.questions[section];
    return question.title;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    TTQuestion *question = self.questionGroup.questions[section];
    return [TTTableViewCellFactory numberOfRowsForQuestion:question];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TTQuestion *question = self.questionGroup.questions[indexPath.section];
    TTAnswer *answer = [self answerForQuestion:question];
    return [TTTableViewCellFactory heightForRowUsingQuestion:question andAnswer:answer tableView:tableView];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TTQuestion *question = self.questionGroup.questions[indexPath.section];
    NSString *reuseIdentifier = [TTTableViewCellFactory reuseIdentifierForQuestion:question];

    TTQuestionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (!cell) {
        cell = [TTTableViewCellFactory questionCellUsingQuestion:question];
    }
    
    if (question.questionType == TTQuestionTypeSingleSelect) {
        
        TTSingleSelectQuestion *singleSelectQuestion = (TTSingleSelectQuestion *)question;
        NSArray *options = singleSelectQuestion.options;
        TTOption *option = options[indexPath.row];
        TTAnswer *answer = [self answerForQuestion:question];
        
        if (answer.value.length > 0) {
            NSInteger value = [answer.value integerValue];
            if (value == option.value) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            } else {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        cell.textLabel.text = option.title;
        
    } else {
        TTAnswer *answer = [self answerForQuestion:question];
        [cell setAnswer:answer];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TTQuestion *question = self.questionGroup.questions[indexPath.section];
    if (question.questionType == TTQuestionTypeDate) {
        
        [self setCurrentQuestion:question];
        [self performSegueWithIdentifier:kTTSequeForDatePickerCollection sender:nil];
        
    } else if (question.questionType == TTQuestionTypeSingleSelect) {
        
        TTSingleSelectQuestion *singleSelectQuestion = (TTSingleSelectQuestion *)question;
        NSArray *options = singleSelectQuestion.options;
        TTAnswer *answer = [self answerForQuestion:singleSelectQuestion];
        
        if (!answer) {
            answer = [[[TTAnswer alloc] init] autorelease];
            answer.questionID = question.questionID;
            [self.answers addObject:answer];
        }
        TTOption *option = options[indexPath.row];
        answer.value = [NSString stringWithFormat:@"%ld", (long)option.value];
        
        [tableView reloadData];
    }
}

#pragma mark - TTTableViewDelegate

- (void)tableView:(UITableView *)tableView didChangeAnswerValue:(NSString *)value forCell:(TTQuestionTableViewCell *)cell
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    // Gather the received answer
    TTQuestion *question = self.questionGroup.questions[indexPath.section];
    TTAnswer *answer = [self answerForQuestion:question];
    if (!answer) {
        answer = [[[TTAnswer alloc] init] autorelease];
        answer.questionID = question.questionID;
        [self.answers addObject:answer];
    }
    answer.value = value;
    
    [tableView beginUpdates];
    [tableView endUpdates];
}

#pragma mark - TTPickerViewControllerDelegate

- (void)viewController:(TTPickerViewController *)controller didSelectDate:(NSDate *)date
{
    TTQuestion *question = self.currentQuestion;
    TTAnswer *answer = [self answerForQuestion:question];
    if (!answer) {
        answer = [[[TTAnswer alloc] init] autorelease];
        answer.questionID = question.questionID;
        [self.answers addObject:answer];
    }
    answer.value = [NSString stringWithFormat:@"%f", date.timeIntervalSince1970];

    [self.tableView reloadData];
    self.currentQuestion = nil;
}

#pragma mark - Helpers

- (TTAnswer *)answerForQuestion:(TTQuestion *)question
{
    TTAnswer *answer = nil;
    for (TTAnswer *currentAnswer in self.answers) {
        if (currentAnswer.questionID == question.questionID) {
            answer = currentAnswer;
            break;
        }
    }
    return answer;
}

@end

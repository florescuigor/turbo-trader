// ---------------------------------------------------------------------------------------------------------------------

#import "TTQuestionTableViewCell.h"

// ---------------------------------------------------------------------------------------------------------------------

// UI representation of a cell used to show text answer
@interface TTTextQuestionTableViewCell : TTQuestionTableViewCell
@end

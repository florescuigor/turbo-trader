// ---------------------------------------------------------------------------------------------------------------------

#import "TTTableViewCellFactory.h"
#import "TTQuestionTableViewCell.h"
#import "TTQuestion.h"
#import "TTAnswer.h"
#import "TTCommon.h"
#import "TTSingleSelectQuestion.h"

// ---------------------------------------------------------------------------------------------------------------------

@implementation TTTableViewCellFactory

+ (NSString *)reuseIdentifierForQuestion:(TTQuestion *)question
{
    return NSStringFromClass([question class]);
}

+ (NSInteger)numberOfRowsForQuestion:(TTQuestion *)question
{
    NSInteger count = 0;
    
    if (question.questionType == TTQuestionTypeDate) {
        return 1;
    } else if (question.questionType == TTQuestionTypeText) {
        return 1;
    } else if (question.questionType == TTQuestionTypeSingleSelect) {
        TTSingleSelectQuestion *singleSelectQuestion = (TTSingleSelectQuestion *)question;
        return singleSelectQuestion.options.count;
    }
    
    return count;
}

+ (CGFloat)heightForRowUsingQuestion:(TTQuestion *)question
                           andAnswer:(TTAnswer *)answer
                           tableView:(UITableView *)tableView
{
    CGFloat height = 44;
    if (question.questionType == TTQuestionTypeText) {
        height = [TTTableViewCellFactory heightForText:answer.value tableView:tableView];
    }
    return height;
}

+ (TTQuestionTableViewCell *)questionCellUsingQuestion:(TTQuestion *)question
{
    NSString *reuseIdentifier = [TTTableViewCellFactory reuseIdentifierForQuestion:question];
    TTQuestionTableViewCell *questionCell = nil;
    
    if (question.questionType == TTQuestionTypeDefault) {
        questionCell = [[[TTQuestionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                      reuseIdentifier:reuseIdentifier] autorelease];
    } else if (question.questionType == TTQuestionTypeText) {
        questionCell = [[[TTQuestionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                      reuseIdentifier:reuseIdentifier] autorelease];
    } else if (question.questionType == TTQuestionTypeSingleSelect) {
        questionCell = [[[TTQuestionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                      reuseIdentifier:reuseIdentifier] autorelease];
    }
    
    return questionCell;
}

#pragma mark - Helpers

+ (CGFloat)heightForText:(NSString *)text tableView:(UITableView *)tableView
{
    CGFloat height = 54;
    UIFont *font = [UIFont systemFontOfSize:14];
    CGSize size = CGSizeMake(tableView.frame.size.width - 38, MAXFLOAT);
    NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin;
    CGRect rect = [text boundingRectWithSize:size options:options attributes:@{NSFontAttributeName : font}  context:nil];
    NSInteger linesCount = rect.size.height / font.lineHeight;
    
    if (linesCount > 1) {
        return height + ((linesCount - 1) * font.lineHeight);
    } else {
        return height;
    }
}

@end
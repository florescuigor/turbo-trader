// ---------------------------------------------------------------------------------------------------------------------

#import "TTQuestion.h"

// ---------------------------------------------------------------------------------------------------------------------

@interface TTDateQuestion : TTQuestion
@property(nonatomic, retain)NSDate *minimumDate;
@property(nonatomic, retain)NSDate *maximumDate;
@end
